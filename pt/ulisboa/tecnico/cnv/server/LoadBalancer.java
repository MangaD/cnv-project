package pt.ulisboa.tecnico.cnv.server;

import java.util.*;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.*;
import com.amazonaws.services.cloudwatch.AmazonCloudWatch;
import com.amazonaws.services.cloudwatch.AmazonCloudWatchClientBuilder;
import com.amazonaws.services.cloudwatch.model.Dimension;
import com.amazonaws.services.cloudwatch.model.Datapoint;
import com.amazonaws.services.cloudwatch.model.GetMetricStatisticsRequest;
import com.amazonaws.services.cloudwatch.model.GetMetricStatisticsResult;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.dynamodbv2.model.Condition;
import com.amazonaws.services.dynamodbv2.model.ResourceNotFoundException;
import java.awt.image.BufferedImage;
import java.net.InetSocketAddress;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;

import com.amazonaws.util.EC2MetadataUtils;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import pt.ulisboa.tecnico.cnv.solver.Coordinate;
import pt.ulisboa.tecnico.cnv.solver.Solver;
import pt.ulisboa.tecnico.cnv.solver.SolverArgumentParser;
import pt.ulisboa.tecnico.cnv.solver.SolverFactory;
import javax.imageio.ImageIO;
import java.net.*;
import java.io.*;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.io.IOUtils;

public class LoadBalancer {

	static AmazonEC2      ec2;
    static AmazonDynamoDB dynamoDB;
    static ConcurrentHashMap<String, Integer> machinesLoad = new ConcurrentHashMap<>(); // <publicIpAdress, TotalLoopsRunning>
    static ConcurrentHashMap<String, Long> lastTriggered = new ConcurrentHashMap<>(); // <instanceID, lastTimeItTriggeredANewInstance>
    static ArrayList<String> toKill = new ArrayList<>();
	static String loadBalancerID = "";
	static final Integer PENDING = 0;
	static final Integer RUNNING = 16;
	static final Integer SHUTTING_DOWN = 32;
	static final Integer TERMINATED = 48; 
	static final Integer STOPPING = 64;
	static final Integer STOPPED = 80;
    static final Integer UPPERBOUND = 270000000;
    static final Integer LOWERBOUND =  67500000;
    static AtomicInteger runningInstances = new AtomicInteger(0);
    static final Integer OFFSET = 1000 * 60; // 60 seconds
    static final Object loadBalancerMutex = new Object();
    static final Object runningInstancesMutex = new Object();

	private static void init() throws Exception {

        /*
         * The ProfileCredentialsProvider will return your [default]
         * credential profile by reading from the credentials file located at
         * (~/.aws/credentials).
         */
        AWSCredentials credentials = null;
        ProfileCredentialsProvider credentialsProvider = new ProfileCredentialsProvider();
        try {
            credentials = credentialsProvider.getCredentials();
        } catch (Exception e) {
            throw new AmazonClientException(
                    "Cannot load the credentials from the credential profiles file. " +
                    "Please make sure that your credentials file is at the correct " +
                    "location (~/.aws/credentials), and is in valid format.",
                    e);
        }
      ec2 = AmazonEC2ClientBuilder.standard().withRegion("us-east-1").withCredentials(new AWSStaticCredentialsProvider(credentials)).build();
      dynamoDB = AmazonDynamoDBClientBuilder.standard()
            .withCredentials(credentialsProvider)
            .withRegion("us-east-1")
            .build();
    }

    private static void printInstances(Set<Instance> instances) {
    	System.out.println("===========================================");
    	System.out.println("Load Balancer instance ID = " + loadBalancerID);
    	for (Instance i: instances) {
			System.out.println("Instance -> " + i.getInstanceId() + " | " + i.getPublicIpAddress() + " | " + i.getState());
		}
		System.out.println("===========================================");
    }

	public static void main(final String[] args) throws Exception {
		System.out.println("===========================================");
        System.out.println("Welcome to LoadBalancer!");
        System.out.println("===========================================");

		init();

		try {
            Set<Instance> instances = new HashSet<Instance>();
            DescribeInstancesResult describeInstancesRequest = ec2.describeInstances();
            List<Reservation> reservations = describeInstancesRequest.getReservations();

            for (Reservation reservation : reservations) {
                instances.addAll(reservation.getInstances());
            }

            //runningInstances.set(1); // at the beggining we only have load balancer running
            printInstances(instances);

            // check number of instances in a non terminated state and get 'this' instance ID
            loadBalancerID = EC2MetadataUtils.getInstanceId();
            int runningI = 0;
            for (Instance i: instances) {
            	int state = i.getState().getCode();
				if (state == RUNNING || state == PENDING)
                    runningI++;
			}

            runningInstances.set(runningI);

			if (runningI > 1) {
            	System.out.println("Turn off all the machines before starting except the one that will be running the load balancer.");
            	return;
            }

            System.out.println("Starting the first WebServer.");
			createWebServer();
            System.out.println("See your instance in the AWS console...");
     
        } catch (AmazonServiceException ase) {
                System.out.println("Caught Exception: " + ase.getMessage());
                System.out.println("Reponse Status Code: " + ase.getStatusCode());
                System.out.println("Error Code: " + ase.getErrorCode());
                System.out.println("Request ID: " + ase.getRequestId());
                System.exit(1);
        }

		//final HttpServer server = HttpServer.create(new InetSocketAddress("127.0.0.1", 8000), 0);        
		final HttpServer server = HttpServer.create(new InetSocketAddress(8000), 0);

		server.createContext("/climb", new MyHandler());

		// be aware! infinite pool of threads!
		server.setExecutor(Executors.newCachedThreadPool());
		server.start();

		System.out.println(server.getAddress().toString());
	}

    private static void createWebServer() {
        RunInstancesRequest runInstancesRequest =
                new RunInstancesRequest();

        IamInstanceProfileSpecification iamIntancePS =
                new IamInstanceProfileSpecification().withName("Dynamo"); // DynamoPermissions

        runInstancesRequest.withImageId("ami-02ed75b14abc26230") // Configure
                .withIamInstanceProfile(iamIntancePS)
                .withInstanceType("t2.micro")
                .withMinCount(1)
                .withMaxCount(1)
                .withKeyName("CNV-lab-AWS")
                .withSecurityGroups("CNV-ssh+http");

        ec2.runInstances(runInstancesRequest); // start new instance

        runningInstances.addAndGet(1);
    }

    static class MyHandler implements HttpHandler {
		@Override
		public void handle(final HttpExchange t) throws IOException {

			// Get the query.
			final String query = t.getRequestURI().getQuery();

			System.out.println("> Query:\t" + query);

            // Break it down into String[].
            final String[] params = query.split("&");

            /*
            for(String p: params) {
                System.out.println(p);
            }
            */

            // Store as if it was a direct call to SolverMain.
            final ArrayList<String> newArgs = new ArrayList<>();
            for (final String p : params) {
                final String[] splitParam = p.split("=");
                newArgs.add("-" + splitParam[0]);
                newArgs.add(splitParam[1]);

                /*
                System.out.println("splitParam[0]: " + splitParam[0]);
                System.out.println("splitParam[1]: " + splitParam[1]);
                */
            }

            newArgs.add("-d");

            // Store from ArrayList into regular String[].
            final String[] args = new String[newArgs.size()];
            int i = 0;
            for(String arg: newArgs) {
                args[i] = arg;
                i++;
            }

            /*
            for(String ar : args) {
                System.out.println("ar: " + ar);
            } */

            SolverArgumentParser ap = null;
            try {
                // Get user-provided flags.
                ap = new SolverArgumentParser(args);
            }
            catch(Exception e) {
                System.out.println(e);
                return;
            }

            System.out.println("> Finished parsing args.");

            // Create solver instance from factory.
            final Solver s = SolverFactory.getInstance().makeSolver(ap);

            try {
                // estimar o custo do pedido
                int estimatedLoops = LoadBalancer.estimateRequestLoops(ap, s);

                // resend http request to another instance
                Set<Instance> instances = new HashSet<Instance>();
                DescribeInstancesResult describeInstancesRequest = ec2.describeInstances();
                List<Reservation> reservations = describeInstancesRequest.getReservations();

                for (Reservation reservation : reservations) {
                    instances.addAll(reservation.getInstances());
                }

                for (Instance i2: instances) {
                    if (!i2.getInstanceId().equals(loadBalancerID) &&
                            i2.getState().getCode() == RUNNING && i2.getPublicIpAddress() != null) {
                        machinesLoad.putIfAbsent(i2.getPublicIpAddress(), 0); // initialize entry on our load tracker hashmap
                    }
                }

                String instanceIP = "";
                String instance_id = "";
                int newLoad;
                synchronized (loadBalancerMutex) {
                    // find less loaded instance
                    int min = Integer.MAX_VALUE;
                    for (Instance ins : instances) {
                        if (!ins.getInstanceId().equals(loadBalancerID)
                                && ins.getState().getCode() == RUNNING
                                && !toKill.contains(ins.getInstanceId())) {
                            int load = machinesLoad.get(ins.getPublicIpAddress());
                            if (load < min) {
                                instanceIP = ins.getPublicIpAddress();
                                instance_id = ins.getInstanceId();
                                min = load;
                            }
                        }
                    }

                    System.out.println("InstanceIP = " + instanceIP + "\nML = " + machinesLoad.get(instanceIP));

                    synchronized (instance_id.intern()) { // Synchronize writes on the specific selected instance
                        // threads reading/writing in the same position
                        newLoad = machinesLoad.get(instanceIP) + estimatedLoops;
                        machinesLoad.put(instanceIP, newLoad);
                    }
                }

                if (newLoad > LoadBalancer.UPPERBOUND) {
                    System.out.println("Upperbound was crossed. Triggering new instance.");
                    upperboundProtocol(instance_id);
                }

                System.out.println("Instance_IP = " + instanceIP + " | NEWLOAD = " + newLoad);

                // Send request
                System.out.println("Redirecting request to instance " + instanceIP + "...");
                URL url = new URL("http://" + instanceIP + ":8000/climb?" + query);
                HttpURLConnection con;
                while (true) {
                    try {
                        con = (HttpURLConnection) url.openConnection();
                        con.setRequestMethod("GET");
                        int responseCode = con.getResponseCode();
                        System.out.println("Sending 'GET' request to URL : " + url);
                        System.out.println("Response Code : " + responseCode);
                        break;
                    } catch (ConnectException ce) {
                        System.out.println("Connection to WebServer refused. Waiting 10 seconds until next attempt.");
                        Thread.sleep(10000); // 10 seconds
                    }
                }
                
                synchronized (instance_id.intern()) { // Going to update load, so it has to be synchronized
                    newLoad = machinesLoad.get(instanceIP) - estimatedLoops;
                    machinesLoad.put(instanceIP, newLoad);
                }

                System.out.println("Instance_IP = " + instanceIP + " | NEWLOAD = " + newLoad);

                if (newLoad < LoadBalancer.LOWERBOUND) {
                    System.out.println("Lowerbound was crossed. Triggering to kill.");
                    lowerBoundProtocol(instance_id, instanceIP);
                }

                // Receive response from WebServer
                InputStream in = con.getInputStream();

                byte[] imageInBytes = IOUtils.toByteArray(in); // convert InputStream to byte[]
                
                File responseFile = null;
                try {
                    final String outPath = ap.getOutputDirectory();

                    final String imageName = s.toString();

                    if(ap.isDebugging()) {
                        System.out.println("> Image name: " + imageName);
                    }
                    final Path imagePathPNG = Paths.get(outPath, imageName);

                    // convert byte[] to BufferedImage
                    ByteArrayInputStream bis = new ByteArrayInputStream(imageInBytes);
                    BufferedImage outputImg = ImageIO.read(bis);

                    ImageIO.write(outputImg, "png", imagePathPNG.toFile());
                    responseFile = imagePathPNG.toFile();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                // Send response to client's browser.
                final Headers hdrs = t.getResponseHeaders();

                t.sendResponseHeaders(200, responseFile.length());

                hdrs.add("Content-Type", "image/png");

                hdrs.add("Access-Control-Allow-Origin", "*");
                hdrs.add("Access-Control-Allow-Credentials", "true");
                hdrs.add("Access-Control-Allow-Methods", "POST, GET, HEAD, OPTIONS");
                hdrs.add("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");

                final OutputStream os = t.getResponseBody();
                Files.copy(responseFile.toPath(), os);

                os.close();

                System.out.println("> Sent response to " + t.getRemoteAddress().toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
		}

        private void lowerBoundProtocol(String instance_id, String instanceIP) {
            synchronized (loadBalancerMutex) {
                if (runningInstances.get() > 2) { //LoadBalancer + Webserver1 + Webserver...
                    synchronized (instance_id.intern()) {
                        if (machinesLoad.get(instanceIP) == 0) {

                            toKill.remove(instance_id); // removes if exists

                            runningInstances.addAndGet(-1);
                            System.out.println("killing instance with ID = " + instance_id);
                            TerminateInstancesRequest request = new TerminateInstancesRequest()
                                    .withInstanceIds(instance_id);

                            ec2.terminateInstances(request);
                        } else {
                            if (!toKill.contains(instance_id))
                                toKill.add(instance_id);
                        }
                    }
                } else
                    System.out.println("One WebServer running only. Can't kill.");
            }
        }

        private void upperboundProtocol(String instanceID) {
		    synchronized (instanceID.intern()) {
                Long time = lastTriggered.get(instanceID);
                if (time == null || (new Date().getTime() - OFFSET) > time) {
                    lastTriggered.put(instanceID, new Date().getTime());
                    createWebServer();
                }
                else {
                    System.out.println("A new WebServer was already triggered by this instance in less than " +
                            "" + OFFSET/1000 + " seconds.");
                }
            }
        }
    }

    private static int estimateRequestLoops(SolverArgumentParser ap, Solver s) {
        String strat = ap.getSolverStrategy().toString();
        System.out.println("Strategy = " + strat);
        System.out.println("StartX = " + s.getStartX());
        System.out.println("StartY = " + s.getStartY());
        System.out.println("X0 = " + s.getX0());
        System.out.println("Y0 = " + s.getY0());
        System.out.println("X1 = " + s.getX1());
        System.out.println("Y1 = " + s.getY1());

        String tableName = (ap.getInputImage()).substring("datasets/".length());
        int estimatedLoops = 0;
        int _x0 = s.getX0();
        int _x1 = s.getX1();
        int _y0 = s.getY0();
        int _y1 = s.getY1();
        int _spx = s.getStartX();
        int _spy = s.getStartY();
        int mapHeight = s.getHeight();
        int mapWidth = s.getWidth();

        /* ASTAR: To estimate an ASTAR request, we will scan all the previous requests with similar 
            areas (+/-10%) and calculate the average, because we found that requests with the same 
            area execute roughly the same number of loops during the execution of the request and 
            take roughly the same time to execute under the same conditions */
        try {
            switch(strat) {
                case "ASTAR":
                    // Scan items using ASTAR strategy and with similar areas
                    int area = (_x1 - _x0) * (_y1 - _y0);
                    HashMap<String, Condition> scanFilter = new HashMap<String, Condition>();
                    Condition condition = new Condition()
                        .withComparisonOperator(ComparisonOperator.BETWEEN.toString())
                        .withAttributeValueList(new AttributeValue().withN(Double.toString(area*0.9)), 
                                                new AttributeValue().withN(Double.toString(area*1.1)));
                    scanFilter.put("area", condition);

                    condition = new Condition()
                        .withComparisonOperator(ComparisonOperator.EQ.toString())
                        .withAttributeValueList(new AttributeValue().withS("ASTAR"));
                    scanFilter.put("strat", condition);

                    ScanRequest scanRequest = new ScanRequest(tableName).withScanFilter(scanFilter);
                    ScanResult scanResult = dynamoDB.scan(scanRequest);
                    List<Map<String,AttributeValue>> result = scanResult.getItems();
                    int count = scanResult.getCount();
                    System.out.println("Count = " + count);
                    
                    if (count == 0) { // no results returned
                        System.out.println("No item found.");
                        break;
                    }

                    int totalLoops = 0;
                    for (Map<String,AttributeValue> map : result) {
                        Set<String> keys = map.keySet();
                        System.out.println("-----------------------");
                        for (String key : keys) {
                            System.out.print(key + " -> " + map.get(key) + " | ");
                            if (key.equals("loops")) {
                                totalLoops += parseValue(map.get(key).toString());
                            }
                        }
                        System.out.println("-----------------------");
                    }

                    estimatedLoops = totalLoops/count; // calculate average of requests with similar areas
                    break;

                /* To estimate a BFS request we will scan all the previous requests that had target points within
                * the area of search of the actual request and calculate the average distance between all of them.
                * The result will be an estimated distance to the "probably" highest target point and then we estimate
                * the area of the rhodus originated by the BFS search starting at the StartPoint when searching all
                * the points within the estimated distance. Finnaly, as the estimated rhodus may grow beyond the
                * search area of the request, we calculate the intersection between the rhodus and the search rectangle*/
                case "BFS":
                    scanFilter = new HashMap<String, Condition>();
                    condition = new Condition() /* get all points between (included) x0 and x1 */
                            .withComparisonOperator(ComparisonOperator.BETWEEN.toString())
                            .withAttributeValueList(new AttributeValue().withN(Integer.toString(_x0)),
                                    new AttributeValue().withN(Integer.toString(_x1)));
                    scanFilter.put("tx", condition);

                    condition = new Condition() /* get all points between (included) y0 and y1 */
                        .withComparisonOperator(ComparisonOperator.BETWEEN.toString())
                        .withAttributeValueList(new AttributeValue().withN(Integer.toString(_y0)),
                                                new AttributeValue().withN(Integer.toString(_y1)));
                    scanFilter.put("ty", condition);

                    scanRequest = new ScanRequest(tableName).withScanFilter(scanFilter);
                    scanResult = dynamoDB.scan(scanRequest);
                    result = scanResult.getItems();
                    count = scanResult.getCount();
                    System.out.println("Count = " + count);
                    
                    if (count == 0) { // no results returned
                        System.out.println("No item found.");
                        break;
                    }

                    double totalDistance = 0 ;

                    for (Map<String,AttributeValue> map : result) {
                        Set<String> keys = map.keySet();
                        System.out.println("-----------------------");
                        int x = -1;
                        int y = -1;
                        for (String key : keys) {
                            System.out.print(key + " -> " + map.get(key) + " | ");
                            if (key.equals("tx"))
                              x = parseValue(map.get(key).toString());
                            if (key.equals("ty"))
                              y= parseValue(map.get(key).toString());
                        }

                        totalDistance += Math.sqrt((_spy - y) * (_spy - y) + (_spx - x) * (_spx - x));

                        System.out.println("-----------------------");
                    }

                    double estimateDistanceToTarget  = (totalDistance / count);
                    double estimatedAreaOfSearchBestCase = (estimateDistanceToTarget*estimateDistanceToTarget*2);
                    int estimatedAreaOfSearchAverageCase = (int) (estimatedAreaOfSearchBestCase*1.5);

                    //double estimatedAreaOfSearchWorstCase = (estimatedAreaOfSearchBestCase*2);
                    /*System.out.println("Estimated area Worst Case = " + estimatedAreaOfSearchWorstCase);
                    System.out.println("Estimated area Best Case = " + estimatedAreaOfSearchBestCase);
                    System.out.println("Estimated Average Case = " + estimatedAreaOfSearchAverageCase);*/

                    /* Remover a area do losango que esta fora da area de procura:
                    Para o fazer, criamos um quadrado centrado no starting point com a mesma area do losango calculado
                    (resultado identico ao que teriamos se rodassemos o losango 45 graus)
                    A area do quadrado criado que interseta com o retangulo de procura e igual. Mas tendo um quadrado,
                    o calculo da intersecao de areas e mais facil de fazer
                     */
                    double squareLdividedby2 = Math.sqrt(estimatedAreaOfSearchAverageCase)/2;
                    double ex0 = Math.max(0, _spx - squareLdividedby2);
                    double ex1 = Math.min(_spx + squareLdividedby2, mapWidth);
                    double ey0 = Math.max(_spy - squareLdividedby2, 0);
                    double ey1 = Math.min(_spy + squareLdividedby2, mapHeight);

                    /*System.out.println("Square created:");
                    System.out.println(ex0);
                    System.out.println(ex1);
                    System.out.println(ey0);
                    System.out.println(ey1);*/

                    double left = Math.max(_x0, ex0);
                    double right = Math.min(_x1, ex1);
                    double top = Math.max(_y0, ey0);
                    double bottom = Math.min(_y1, ey1);

                    /*System.out.println("\n Intersection:");
                    System.out.println(left);
                    System.out.println(right);
                    System.out.println(top);
                    System.out.println(bottom);*/

                    double intersectionAreaBFS;
                    if (left < right && bottom > top) // intersection is not empty if
                        intersectionAreaBFS = (bottom-top) * (right-left);
                    else
                        intersectionAreaBFS = 0;

                    System.out.println("Estimated area after intersection (intersectionAreaBFS) = " + intersectionAreaBFS);

                    //Loops = estimated area to search * BFS loops per iteration
                    estimatedLoops = (int) (intersectionAreaBFS * 1035.3);

                    break;

                /*DFS: Obter todos os requests que tiveram targets dentro da area de procura do request atual
                * Selecionar o target que aparece mais vezes e assumir que esse sera o target do request atual
                * Calcular a area de procura que uma DFS search faria comecando no StartingPoint e acabando no
                * ponto estimado*/
                case "DFS":
                    scanFilter = new HashMap<String, Condition>();
                    condition = new Condition() /* get all points between (included) x0 and x1 */
                            .withComparisonOperator(ComparisonOperator.BETWEEN.toString())
                            .withAttributeValueList(new AttributeValue().withN(Integer.toString(_x0)),
                                    new AttributeValue().withN(Integer.toString(_x1)));
                    scanFilter.put("tx", condition);

                    condition = new Condition() /* get all points between (included) y0 and y1 */
                            .withComparisonOperator(ComparisonOperator.BETWEEN.toString())
                            .withAttributeValueList(new AttributeValue().withN(Integer.toString(_y0)),
                                    new AttributeValue().withN(Integer.toString(_y1)));
                    scanFilter.put("ty", condition);

                    scanRequest = new ScanRequest(tableName).withScanFilter(scanFilter);
                    scanResult = dynamoDB.scan(scanRequest);
                    result = scanResult.getItems();
                    count = scanResult.getCount();
                    System.out.println("Count = " + count);

                    if (count == 0) { // no results returned
                        System.out.println("No item found.");
                        break;
                    }

                    Map< Coordinate, Integer > coordinateMap = new HashMap<>();

                    for (Map<String,AttributeValue> map : result) {
                        Set<String> keys = map.keySet();
                        System.out.println("-----------------------");
                        int x= -1;
                        int y=-1;
                        for (String key : keys) {
                            System.out.print(key + " -> " + map.get(key) + " | ");
                            if (key.equals("tx"))
                                x = parseValue(map.get(key).toString());
                            if (key.equals("ty"))
                                y= parseValue(map.get(key).toString());
                        }

                        Coordinate _coord = new Coordinate(x,y);
                        boolean exists = false ;

                        for (Map.Entry< Coordinate, Integer> entry : coordinateMap.entrySet()) {
                            Coordinate coordinate = entry.getKey();

                            if( _coord.equals(coordinate)){
                                Integer contador = entry.getValue() + 1 ;
                                coordinateMap.put( coordinate, contador);
                                exists = true;
                                break;
                            }

                        }

                        if(!exists)
                            coordinateMap.put( _coord, 1);

                        System.out.println("-----------------------");
                    }

                    if (coordinateMap.size() == 0) { // no previous target points found in the search area
                        System.out.println("No target points found!");
                        break;
                    }

                    Map.Entry<Coordinate, Integer> maxEntry = null;
                    for (Map.Entry< Coordinate, Integer> entry : coordinateMap.entrySet()) {

                            if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0) {
                                maxEntry = entry;
                            }
                    }

                    Coordinate estimHigherCoord = maxEntry.getKey();
                    int estimX = estimHigherCoord.getX();
                    int estimY = estimHigherCoord.getY();
                    int searchHeight = _y1 - _y0;
                    int deltaX = Math.abs(estimX - _spx);

                    int estimatedAreaOfSearchDFS;
                    if (estimX > _spx) { // highest point estimado esta a direita do ponto de partida
                        estimatedAreaOfSearchDFS = deltaX*searchHeight;
                    }

                    else { // o ponto mais alto estimado esta a esquerda, e por isso a DFS vai procurar toda a zona
                        // a direita, e depois a zona a esquerda ate encontrar o ponto
                        if (estimX == _spx && estimY == _spy) {
                            estimatedAreaOfSearchDFS = 0; // o proprio ponto de partida e igual ao target estimado
                        }
                        else {
                            estimatedAreaOfSearchDFS = (_x1 - _spx)*searchHeight + deltaX*searchHeight;
                        }
                    }
                    System.out.println("Estimated Coord = (" + estimHigherCoord.getX()+","+estimHigherCoord.getY()+")");
                    System.out.println("Estimated Area Of Serch = " + estimatedAreaOfSearchDFS);

                    //Loops = estimated area to search * DFS loops per iteration
                    estimatedLoops = (int) (estimatedAreaOfSearchDFS * 1979.7);
                    break;

                default:
                    break;
            }
        } catch(ResourceNotFoundException e) {
            // table does not exist (first request for the map)
            // do nothing, WebServer will create it
        }

        System.out.println("================= Estimated Loops = " + estimatedLoops + " =================");
        return estimatedLoops;
    }

    private static int parseValue(String s) {
        String res = s.substring("{N: ".length(), s.length()-",}".length());
        return Integer.parseInt(res);
    }
}
