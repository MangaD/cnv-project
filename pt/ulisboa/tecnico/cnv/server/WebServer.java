package pt.ulisboa.tecnico.cnv.server;

import java.util.Map;
import java.util.Set;
import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.dynamodbv2.model.Condition;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.DescribeTableRequest;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.PutItemResult;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.amazonaws.services.dynamodbv2.model.ScalarAttributeType;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.amazonaws.services.dynamodbv2.model.TableDescription;
import com.amazonaws.services.dynamodbv2.util.TableUtils;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.Executors;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import pt.ulisboa.tecnico.cnv.solver.Solver;
import pt.ulisboa.tecnico.cnv.solver.SolverArgumentParser;
import pt.ulisboa.tecnico.cnv.solver.SolverFactory;
import java.io.*;
import javax.imageio.ImageIO;

public class WebServer {

	static AmazonDynamoDB dynamoDB;

	/**
     * The only information needed to create a client are security credentials
     * consisting of the AWS Access Key ID and Secret Access Key. All other
     * configuration, such as the service endpoints, are performed
     * automatically. Client parameters, such as proxies, can be specified in an
     * optional ClientConfiguration object when constructing a client.
     *
     * @see com.amazonaws.auth.BasicAWSCredentials
     * @see com.amazonaws.auth.ProfilesConfigFile
     * @see com.amazonaws.ClientConfiguration
     */
    private static void init() throws Exception {
        /*
         * The ProfileCredentialsProvider will return your [default]
         * credential profile by reading from the credentials file located at
         * (~/.aws/credentials).
         */
        /*ProfileCredentialsProvider credentialsProvider = new ProfileCredentialsProvider();
        try {
            credentialsProvider.getCredentials();
        } catch (Exception e) {
            throw new AmazonClientException(
                    "Cannot load the credentials from the credential profiles file. " +
                    "Please make sure that your credentials file is at the correct " +
                    "location (~/.aws/credentials), and is in valid format.",
                    e);
        }*/
        dynamoDB = AmazonDynamoDBClientBuilder.standard()
			//.withCredentials(credentialsProvider)
            .withRegion("us-east-1")
            .build();
    }

	public static void main(final String[] args) throws Exception {

		init();

		//final HttpServer server = HttpServer.create(new InetSocketAddress("127.0.0.1", 8000), 0);

		final HttpServer server = HttpServer.create(new InetSocketAddress(8000), 0);



		server.createContext("/climb", new MyHandler());

		// be aware! infinite pool of threads!
		server.setExecutor(Executors.newCachedThreadPool());
		server.start();

		System.out.println(server.getAddress().toString());
	}

	static class MyHandler implements HttpHandler {
		@Override
		public void handle(final HttpExchange t) throws IOException {

			// Get the query.
			final String query = t.getRequestURI().getQuery();

			System.out.println("> Query:\t" + query);

			// Break it down into String[].
			final String[] params = query.split("&");

			/*
			for(String p: params) {
				System.out.println(p);
			}
			*/

			// Store as if it was a direct call to SolverMain.
			final ArrayList<String> newArgs = new ArrayList<>();
			for (final String p : params) {
				final String[] splitParam = p.split("=");
				newArgs.add("-" + splitParam[0]);
				newArgs.add(splitParam[1]);

				/*
				System.out.println("splitParam[0]: " + splitParam[0]);
				System.out.println("splitParam[1]: " + splitParam[1]);
				*/
			}

			newArgs.add("-d");

			// Store from ArrayList into regular String[].
			final String[] args = new String[newArgs.size()];
			int i = 0;
			for(String arg: newArgs) {
				args[i] = arg;
				i++;
			}

			/*
			for(String ar : args) {
				System.out.println("ar: " + ar);
			} */

			SolverArgumentParser ap = null;
			try {
				// Get user-provided flags.
				ap = new SolverArgumentParser(args);
			}
			catch(Exception e) {
				System.out.println(e);
				return;
			}

			System.out.println("> Finished parsing args.");

			// Create solver instance from factory.
			final Solver s = SolverFactory.getInstance().makeSolver(ap);

			byte[] imageInBytes = null;
			try {

				final BufferedImage outputImg = s.solveImage();

				int loops = 0;
				// get loops made for this request
				try {
					System.out.println("\n Current Thread ID = " + Thread.currentThread().getId() + "\n");
					File log = new File("logs/log" + Thread.currentThread().getId() + ".txt");
					BufferedReader br = new BufferedReader(new FileReader(log));
		            loops = Integer.parseInt(br.readLine()); // number of loops
		            System.out.println("================= Loops WebServer " + loops + " =================");
		        } catch (Exception e) {
		        	System.out.println("Something went wrong. Logs folder does not exist?");
		        }

		        // Persist request info in Dynamo Database
		        // Should we persist when StartingPoint = Target ?
		        WebServer.persistRequest(ap, s, loops);

		        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
				ImageIO.write(outputImg, "png", outputStream);
				imageInBytes = outputStream.toByteArray();

			} catch (final FileNotFoundException e) {
				e.printStackTrace();
			} catch (final IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}

			// Send response to LoadBalancer.
			final Headers hdrs = t.getResponseHeaders();

			t.sendResponseHeaders(200, imageInBytes.length);

			hdrs.add("Access-Control-Allow-Origin", "*");
			hdrs.add("Access-Control-Allow-Credentials", "true");
			hdrs.add("Access-Control-Allow-Methods", "POST, GET, HEAD, OPTIONS");
			hdrs.add("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");

			final OutputStream os = t.getResponseBody();
			os.write(imageInBytes);
			os.flush();
			os.close();

			System.out.println("> Sent response to " + t.getRemoteAddress().toString());
		}
	}

	public static void persistRequest(SolverArgumentParser ap, Solver s, int loops) {
		try {
			String strat = ap.getSolverStrategy().toString();
			System.out.println("Strategy = " + strat);
			System.out.println("StartX = " + s.getStartX());
			System.out.println("StartY = " + s.getStartY());
			System.out.println("X0 = " + s.getX0());
			System.out.println("Y0 = " + s.getY0());
			System.out.println("X1 = " + s.getX1());
			System.out.println("Y1 = " + s.getY1());
			System.out.println("Target X = " + s.getTargetX());
			System.out.println("Target Y = " + s.getTargetY());
			System.out.println("InputImage = " + ap.getInputImage());
			int area = (s.getX1()-s.getX0()) * (s.getY1() - s.getY0());
			System.out.println("area = " + area);

			// remove path, keep only map file name
			String tableName = (ap.getInputImage()).substring("datasets/".length());

			// Create a table with a primary hash key named 'zone'
			// What distinguishes two requests are the zone of the request, the starting point and the strategy used
			// If two requests have the same zone and starting point, then they are equal
			// We will only put the zone and strategy as primary key. Requests with the same zone and different
			// starting points will calculate the average of loops based on the previous stored result
            CreateTableRequest createTableRequest = new CreateTableRequest().withTableName(tableName)
                .withKeySchema(new KeySchemaElement().withAttributeName("zonestrat").withKeyType(KeyType.HASH))
                .withAttributeDefinitions(new AttributeDefinition().withAttributeName("zonestrat").withAttributeType(ScalarAttributeType.S))
                .withProvisionedThroughput(new ProvisionedThroughput().withReadCapacityUnits(1L).withWriteCapacityUnits(1L));

            // Create table if it does not exist yet
            TableUtils.createTableIfNotExists(dynamoDB, createTableRequest);
            // wait for the table to move into ACTIVE state
            TableUtils.waitUntilActive(dynamoDB, tableName);

            // Add an item
            String zonestrat = ""+s.getX0()+","+s.getY0() + "|" + s.getX1()+","+s.getY1()+strat;
            Map<String, AttributeValue> item = 
    									newItem(zonestrat, 
										strat, 
										s.getTargetX(), s.getTargetY(), 
										area, loops, 1);

    		PutItemResult putItemResult = null;
    		PutItemRequest putItemRequest = null;
    		try {
	            putItemRequest = new PutItemRequest(tableName, item)
	            	.withConditionExpression("attribute_not_exists(zonestrat)");
	            	// put only if there isn't an entry with the value zonestrat already present
	            	
	            putItemResult = dynamoDB.putItem(putItemRequest);

            } catch(ConditionalCheckFailedException ex) {
		        System.out.println("Colision.");
		        if (s.getStartX() == s.getTargetX() && s.getStartY() == s.getTargetY()) {
		        	System.out.println("StartingPoint = Target. Lets not persist so it doesn't ruin our average.");
		        	return;
		        }

		        // else
				HashMap<String, AttributeValue> pkey = new HashMap<String, AttributeValue>();
	            pkey.put("zonestrat", new AttributeValue().withS(zonestrat));
	            GetItemRequest getItemRequest = new GetItemRequest()
	                .withTableName(tableName)
	                .withKey(pkey)
	                .withProjectionExpression("loops, collisionCounter");
	            
	            Map<String,AttributeValue> result = dynamoDB.getItem(getItemRequest).getItem();

	            // Get response values
				int rLoops = 0;
	            int collisionCounter = 0;

			    if (result != null) {
			        Set<String> keys = result.keySet();
			        for (String key : keys) {
			        	if (key.equals("loops")) {
			        		rLoops = parseValue(result.get(key).toString());
			        	}
			        	else { // key == collisionCounter
			        		collisionCounter = parseValue(result.get(key).toString());
			        	}
			        }
			    } else {
			        System.out.format("No item found with the key %s!\n", zonestrat);
			    }

		        // lets replace the loops entry of the table by the average between the loops and the
		        // obtained loops for the received request (which is the same but with a different starting point)
		        long newLoops = (rLoops * collisionCounter + loops) / (collisionCounter+1);

			    if (newLoops < 0) { // stack overflow
					return;
			    }

		        Map<String, AttributeValue> replaceItem = 
    									newItem(zonestrat, 
										strat, 
										s.getTargetX(), s.getTargetY(), 
										area, newLoops, collisionCounter + 1);

		        putItemRequest = new PutItemRequest(tableName, replaceItem);
	            putItemResult = dynamoDB.putItem(putItemRequest);
		    }

        } catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which means your request made it "
                    + "to AWS, but was rejected with an error response for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which means the client encountered "
                    + "a serious internal problem while trying to communicate with AWS, "
                    + "such as not being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
        } catch (Exception e) {
        	e.printStackTrace();
        }
    }

    private static Map<String, AttributeValue> newItem(String zonestrat, String strat, 
    													int tx, int ty, 
    													int area, long loops, int collisionCounter) {
        Map<String, AttributeValue> item = new HashMap<String, AttributeValue>();
        item.put("zonestrat", new AttributeValue(zonestrat));
        item.put("strat", new AttributeValue(strat));
        item.put("tx", new AttributeValue().withN(Integer.toString(tx)));
        item.put("ty", new AttributeValue().withN(Integer.toString(ty)));
        item.put("area", new AttributeValue().withN(Integer.toString(area)));
        item.put("loops", new AttributeValue().withN(Long.toString(loops)));
        item.put("collisionCounter", new AttributeValue().withN(Integer.toString(collisionCounter)));
        return item;
	}

	private static int parseValue(String s) {
		String res = s.substring("{N: ".length(), s.length()-",}".length());
		return Integer.parseInt(res);
	}
}
