package pt.ulisboa.tecnico.cnv.server;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.awt.image.BufferedImage;
import java.net.InetSocketAddress;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.Executors;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import javax.imageio.ImageIO;
import java.net.*;
import java.io.*;
import javax.xml.ws.Response;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.Date;
import java.util.Random;

public class RequestSender {

	public static void main(final String[] args) throws Exception {
		System.out.println("===========================================");
        System.out.println("Welcome to RequestSender!");
        System.out.println("===========================================");

        //Thread t = new StatsTest();
        //t.start();

        int MAX = 512;

        for (int i = 0; i<450; i++) {
            try {
                Random random = new Random();
                int x0 = random.nextInt(MAX);
                int y0 = random.nextInt(MAX);

                int x1 = random.nextInt(MAX + 1 - x0) + x0;
                int y1 = random.nextInt(MAX + 1 - y0) + y0;

                //int randomNumber = random.nextInt(max + 1 - min) + min;
                int spx = random.nextInt(x1 - x0) + x0;
                int spy = random.nextInt(y1 - y0) + y0;

                System.out.println("X0 = " + x0);
                System.out.println("Y0 = " + y0);
                System.out.println("X1 = " + x1);
                System.out.println("Y1 = " + y1);
                System.out.println("XS = " + spx);
                System.out.println("YS = " + spy);
                String strat = "BFS";
                String loadbalancerIp = "18.232.64.216";
                simple("http://"+loadbalancerIp+":8000/climb?w=512&h=512&x0="+x0+"&x1=" + x1 + "&y0="+ y0 +"&y1="+ y1 +"&xS="+ spx +"&yS="+spy+"&s="+strat+"&i=datasets/RANDOM_HILL_512x512_2019-02-27_09-46-42.dat");
            }catch(Exception e) {
                //
            }
        }

        /*ExecutorService executor = Executors.newFixedThreadPool(10);
        for (int i = 0; i<10 ; i++) {
        	Future<Response> response = executor.submit(new Request());
        }*/

/*
        try {
            Thread.sleep(10000);
        } catch (Exception e) {}

        simple();*/
	}

	public static void simple(String _url) {
		try {
            URL url = new URL(_url);
            Date start = new Date();
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            int responseCode = con.getResponseCode();
            System.out.println("Sending 'GET' request to URL : " + url);
            System.out.println("Response Code : " + responseCode);
            Date end = new Date();
            long d = end.getTime()-start.getTime();
            //System.out.println( " -----> First Simple Time = " + d);
        } catch (Throwable e) {
            e.printStackTrace();
            System.exit(1);
        }
	}
}