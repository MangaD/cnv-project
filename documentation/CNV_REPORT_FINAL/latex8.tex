
%
%  $Description: Author guidelines and sample document in LaTeX 2.09$ 
%
%  $Author: ienne $
%  $Date: 1995/09/15 15:20:59 $
%  $Revision: 1.4 $
%

\documentclass[times, 10pt,twocolumn]{article} 
\usepackage{latex8}
\usepackage{times}
\usepackage{titlesec}
\usepackage[ruled,linesnumbered]{algorithm2e}
\usepackage{graphicx}
\usepackage{multirow}

\setcounter{secnumdepth}{4}

\titleformat{\paragraph}
{\normalfont\normalsize\bfseries}{\theparagraph}{1em}{}
\titlespacing*{\paragraph}
{0pt}{3.25ex plus 1ex minus .2ex}{1.5ex plus .2ex}

%\documentstyle[times,art10,twocolumn,latex8]{article}

%------------------------------------------------------------------------- 
% take the % away on next line to produce the final camera-ready version 
\pagestyle{empty}

%------------------------------------------------------------------------- 
\begin{document}

\title{HillClimbing@Cloud (Final) - Group 20}

\author{David Gonçalves\\david.s.goncalves@tecnico.ulisboa.pt\\
% For a paper whose authors are all at the same institution, 
% omit the following lines up until the closing ``}''.
% Additional authors and addresses can be added with ``\and'', 
% just like the second author.
\and
Ivan Zarro\\ivan.zarro@tecnico.ulisboa.pt\\
\and
Samuel Santos\\samuel.c.santos@tecnico.ulisboa.pt\\
Instituto Superior Técnico\\
}

\maketitle
\thispagestyle{empty}


%------------------------------------------------------------------------- 
\Section{Introduction}

The goal of this project was to design and develop an elastic cluster of web servers that is able to execute a simple e-science related function: to find the maximum value on simplified height-maps (maps) using algorithms such as DFS, BFS and A*.  The system will receive a stream of web requests from users. Each request is for finding the maximum point on a given map, providing the
coordinates of the start position and a search rectangle within the height-map. In the end, it displays the height-map and the computed path (in gray-scale) to reach the maximum, using hill-climbing.

To have scalability, good performance and efficiency, the system will attempt to optimize the selection
of the cluster node for each incoming request and to optimize the number of active nodes in the cluster using the Load Balancer and the Auto-Scaler

%------------------------------------------------------------------------- 
\Section{Implementation}

%------------------------------------------------------------------------- 
\SubSection{Code instrumentation}

We used BIT tool to instrument all the classes inside the ‘solver’ package. The metric that we decided to output once a request is made was the number of loops made during the execution of a request. As the project is mainly related with search algorithms, such as BFS, DFS and ASTAR, we thought that this would be a good metric to reflect the cost of a request, because all these algorithms are based on loops that are iterating over a graph until they find the node they’re searching for. This means that requests that take more time and resources, will be translated in a higher number of loops, and if we succeed in estimating this information, we will have a good hunch of what will be the real cost of a request.

The implementation of the instrumentor can be found in 'BIT/samples/BranchTakenCounter.java'.

%------------------------------------------------------------------------- 
\SubSection{Load balancer}

Every request made by a user is sent to an instance which will be running the load balancer code. The load balancer is a module responsible for estimating the cost of a request, finding the most suitable instance running the WebServer to process the request and redirecting the answer to the client. It is a multithreaded server that can process several requests at the same time.

The load balancer possesses a hashmap that stores the number of estimated loops running in each different instance. When it receives a new request, it will estimate the cost of the request by running an algorithm that will be discussed in a later section. After having an estimate of what will be the cost (number of loops that will be made) of the request, it searches for the instance on the hashmap that has fewer loops running (less loaded instance), redirects the request to it and updates the hashmap incrementing the number of total loops running on that instance. When it receives the answer of the redirected request, it will decrement the estimated cost on the same machine.
This will create an abstraction of the load of an instance in a given moment in time.

%------------------------------------------------------------------------- 
\SubSection{Web server}

Every instance, besides the instance that is responsible for the load balancing, is running the WebServer code. This is where a request is processed, and the solution path of the problem is discovered. It is a multi-threaded server that can process multiple requests at the same time.

The responsibilities that are assigned to this module are the calculation of a solution to the received requests, and the persistence of some information about the requests processed in the DynamoDB, so that the load balancer can improve its estimations on future requests.

Whenever a request is solved, the WebServer will create a table with the name of the request’s map (if it doesn’t exist already) and it will persist a new entry with the fields (zonestrat, strategy, targetX, targetY, area, loops, collisionCounter). We decided to persist information about the target point to estimate requests that use BFS or DFS strategy and about the area to estimate requests that use the A* strategy, that will be discussed in a later section. The ‘zonestrat’ is the primary key of a table, and it is the concatenation of the zone of search “(x0,y0|x1,y1)” with a string that represents the strategy used. This will make that any request that has the same zone of search for a given strategy will not be written twice, even if it uses different starting points. If a request with a primary key that has already been processed is made, the WebServer will calculate the average of loops of all the previous requests plus the new one (and that is why we have the field ‘collisionCounter’) and it will replace the number of loops on that entry.

As you will see later in the report, some of the entries on the table will only not be used to estimate every request, for instance, the ‘area’ and the ‘collisionCounter’ values will only be used to estimate A* requests and the target information will only be used for requests that use DFS and BFS strategies, this means that we will have some entries in the table that will only be “wasting” memory. But as memory is so cheap nowadays, we decided that it wouldn’t be a problem to be concerned about.

%------------------------------------------------------------------------ 
\SubSection{Estimate request's cost}

As we’ve seen already, the load balancer has to estimate the cost of the received requests in order to distribute the load across the instances in a balanced way.
After analyzing several requests and thinking about how the different strategies operate, we noticed that we would be able to obtain more accurate estimates if we estimated differently for each strategy.
This section explains how we decided to calculate the estimation for each different strategy.

%------------------------------------------------------------------------- 

\subsubsection{ASTAR}

After running a few tests, we’ve noticed that the number of loops made during the execution of a request using A* algorithm is tightly correlated to the size of the area we’re performing the search on. The larger the area the more loops the process runs, and requests with the same area of search will execute roughly the same number of loops, even if they are at different zones of the map. We’ve also noticed that the number of loops using this strategy varies with the distance between the starting point and the target, due to the number of loops it takes to draw the path on the solution’s map, but the impact on the total loops is not significant at all. Based on this, we created the following algorithm: \\When the load balancer receives an A* request, it will calculate the loop's average of all the previous requests made on the same map that used the A* strategy and had an area of more or less 10\% of the area of the actual request. To do so, we query the database to retrieve all the requests that fit into these conditions on the table with the name of the request’s map name, and the calculated result will be the estimated number of loops for the given request.

%-------------------------------------------------------------------------

\subsubsection{BFS}
By theory, we already knew that the BFS algorithm starts on a graph node, which in this case is the starting point, and explores all the neighbor nodes at the present depth prior to moving on to the nodes at the next depth level. Based on this, we thought that we could accurately estimate the cost a BFS request by knowing the distance between the starting point and the target point and by calculating the area of the diamond resulting from that BFS search. Having the diamond area, that corresponds to the number of explored pixels, we can estimate the total number of loops made by multiplying it by the number of loops made (on average) on each iteration of a BFS search, which we can easily estimate by finding the correlation between the search area and the number of loops made for that area.\\
Since we do not know the target point before running the solver, we made the WebServer store in the database the target points that were found on each request. With this information, when the load balancer receives a new request, it retrieves all the requests that had target points within the zone's search rectangle and calculates de average distance between the starting point and all the target points found. This way, the target points that appear more times, thus, more likely to be the real targets, will have more weight on the distance estimate calculus. Finnaly, after getting the total area of the diamond with some mathematical logic, we intersect it with the zone's search area to cut off the area of the pixels that surpass it (and that would not be explored).

%-------------------------------------------------------------------------

\subsubsection{DFS}
By theory, we already knew that the DFS algorithm starts at a given node, in this case, the starting point, and explores as far as possible along each branch before backtracking. In the project’s case, a branch is a direction (up, down, left, right). If we found out what directions were prioritized by the algorithm, we could know which area within the request’s search zone would be explored first, and thus, estimate the loops it would take to find the target point.
During our tests, we’ve verified that the solver prioritizes the directions in the order: down \textgreater up \textgreater right \textgreater left.\\
So we decided to, just like what we do when estimating BFS requests, retrieve all the target points within the search area from previous requests, select the target point that appears more times and assume that as the future target point. Then we calculate the cost of the request based on the area between the starting point and the estimated target point. If the target is on a pixel with a higher X coordinate than the starting point, then we know that the algorithm will explore nearly all the pixels that are between the starting and the target point. And once again, we can estimate the cost by multiplying the area to be explored by the number of loops made (on average) in each iteration of a DFS search.

\begin{figure}
%\includegraphics{images/DFS.png}
\includegraphics[width=\linewidth]{images/DFS.png}
\caption{Comparison of the search areas when the target of a DFS request is to the right of the starting point and when it is to the left}
\end{figure}

If the target point has a lower X coordinate than the starting point, then we know that all the search area at the right of the starting point will be explored before moving on to the left area (because right is prioritized over left), thus, the total area will be (x1 – Spx) * (y1-y0) + (Spx – x0) * (y1-y0) (see Figure 1).

%-------------------------------------------------------------------------

\SubSection{Auto Scaler}
To do an efficient elastic scaling of the instances we tried to find two loop bounds (upper and lower) that would indicate whether a machine is already being overloaded enough to justify the instantiation of a new machine or, on the other hand, that it is so ‘freed’ that its existence is not justifiable.
When the upper bound is crossed, we check if the same machine has already triggered an instantiation in a recent past to avoid being constantly instantiating new machines while the already instantiated machine is not in a running state.
When the lower bound is crossed we don’t immediately kill the machine because there may still be requests executing on it. We also thought about the optimization of, before creating a new instance, check if there is an instance marked to kill and remove that flag, saving startup time. Although it looked like a simple optimization, in practice, it would require locks that would put more overhead on the load balancer taking out most of the benefit of having it multithreaded, and since the load balancer is not replicated, we decided not to do it.

\begin{algorithm}[ht]
\SetAlgoLined
  Event receive request:\\{
	estimate = estimateRequestCost()\;
	instance = search instance with lower load \&\& !toKill.contains(instance) \&\& instance.state == Running\;
	instance.loops + = estimate\;
	\If{if instance.loops\textgreater upperBound}{
		\If{lastTimeTriggered[instance]\textgreater  60 seconds}{
			lastTimeTriggered[instance] = now\;
			start new WebServer\;
		}
	}
	redirect Request to instance\;
	receive answer\;
	instance.loops - = estimate\;
	
	\If{if instance.loops\textless  lower bound}{
		\eIf{instance.loops == 0}{
			toKill.remove(instance)\;
			SendTerminateInstanceRequest(instance)\;
		}{
			toKill.add(instance)\;	
		}
	}

	send answer to client\;
}
\caption{AutoScaler Algorithm Pseudocode }
\end{algorithm}


%------------------------------------------------------------------------- 
\SubSection{Evaluation}

%------------------------------------------------------------------------- 

\subsubsection{Instrumentor choice}
In this test, we made different requests using ASTAR strategy with gradually larger complexity in similar environments (same machine, one request at a time, no other processes running) to test the correlation between the number of loops and the time taken to solve the request.
We can observe in Figure 2, that, although it scales differently, the number of loops is a metric that reflects well the complexity of the request, as they grow with similar curves.
\begin{figure}
%\includegraphics[scale=0.65]{images/timevsloops3.png}
\includegraphics[width=\linewidth]{images/timevsloops3.png}
\caption{Analysis of number of loops made with gradually more complex requests.}
\end{figure}

%------------------------------------------------------------------------- 

\subsubsection{Loops vs Strategy}
In this test, we made different requests using different parameters and strategies (request 1 and 4 used strategy ASTAR, request 2 and 5 used BFS, and 3 and 6 used DFS) in similar environments (same machine, one request at a time, no other processes running) to compare if requests that will execute the same number of loops using different strategies would be translated in requests that would take the same time to execute.\\
\begin{table}[h!]
\begin{tabular}{|r|r|r|r|}
\hline
                                                                                      &Strategy & Time(ms)        & \multicolumn{1}{|c|}{Loops} \\
\hline
\multicolumn{1}{|c|}{\multirow{3}{*}{Small Request}} & ASTAR   & $\sim$4159     & 10 975 924                \\
\multicolumn{1}{|c|}{}                                                    & BFS        & $\sim$4242     & 10 991 016                \\
\multicolumn{1}{|c|}{}                                                    & DFS        & $\sim$4294    & 10 934 084                \\
\hline
\multirow{3}{*}{Large Request}                                 & ASTAR    & $\sim$25850 & 69 068 930                \\
                                                                                     & BFS        & $\sim$25750  & 69 062 142                \\
                                                                                     & DFS        & $\sim$25759  & 69 059 911               \\
\hline
\end{tabular}
\caption{Difference in loops between different strategies}
\end{table}
As we can see in Table 1, the answer is yes, which lets us conclude that we can treat each request equally, that is, no strategy is heavier than the other.
%------------------------------------------------------------------------- 

\subsubsection{Overhead}
We made this test by making different requests with gradually larger zones of search. As we can observe in Table 2, the overhead introduced by running the instrumented code that counts the loops when comparing to the original solver code is around 16\%, which we don’t think that is worriedly big.
\begin{table}[h!]
\begin{tabular}{|r|r|r|r|}
\hline
       & Time w/ BIT  & Time w/o BIT & Overhead   \\
\hline
Req. 1 & $\sim$4574   & $\sim$3903   & $\sim$17\% \\
Req. 2 & $\sim$26297  & $\sim$22646  & $\sim$16\% \\
Req. 3 & $\sim$102934 & $\sim$88772  & $\sim$16\% \\
Req. 4 & $\sim$409689 & $\sim$358545 & $\sim$14\% \\
\hline
\end{tabular}
\caption{Overhead analysis}
\end{table}

%------------------------------------------------------------------------- 
\subsubsection{BFS Loops per Area}

In this test we made different requests to the WebServer with different solutions paths in order to check whether or not there exists a correlation between the total number of pixels that would be search by the BFS algorithm and the complexity of the request (number of loops made).\\
\begin{figure}
%\includegraphics[ scale=0.5]{images/astarperformance.png}
\includegraphics[width=\linewidth]{images/BFS_linear.png}
\caption{Analysis of the correlation between the number of loops made and the number of pixels explored on BFS requests.}
\end{figure}
As expected, we confirmed that the complexity of the a BFS request grows linearly with the number of pixels that it has to explore. As we were already estimating the distance to the target point once a request reaches the load balancer, this allowed us to use the obtained formula (see Figure 3) to accurately estimate the future loops for the BFS requests.

%------------------------------------------------------------------------- 
\subsubsection{DFS Loops per Area}
In this test we made different requests to the WebServer with different solutions paths in order to check whether or not there exists a correlation between the total number of pixels that would be search by the DFS algorithm and the complexity of the request (number of loops made).\\
\begin{figure}
%\includegraphics[ scale=0.5]{images/astarperformance.png}
\includegraphics[width=\linewidth]{images/DFS_linear.png}
\caption{Analysis of the correlation between the number of loops made and the number of pixels explored on DFS requests.}
\end{figure}
By observing Figure 4, we can see that we start having outlier points when we reach areas of DFS search of ~240k pixels. This is something that we weren’t expecting. But as those requests would be a minority, assuming that we will get requests with random parameters, and are influencing the R\textsuperscript{2} value significantly (by comparing the orange and the blue regression lines),  we decided to don’t take them into account and estimate the loops of DFS requests based on the orange equation. This will make us estimate most of the requests more accurately, and overestimate big requests (something like 490*490 or equivalent total pixels to search), which is not necessarily bad, because the worst thing that can happen is to trigger a new instance, which we would probably have to do anyway once the next request came.

%------------------------------------------------------------------------- 
\subsubsection{A* Estimate Performance}
In this test we created a script that sent more than four hundred different requests with random (valid) values on the parameters upper-left corner, lower-right corner and starting point, all using the A* strategy on a map with dimensions 512x512, in order to calculate the deviation between the estimated number of loops and the actual number of loops executed on a request.
\begin{figure}
%\includegraphics[ scale=0.5]{images/astarperformance.png}
\includegraphics[width=\linewidth]{images/astarperformance.png}
\caption{Analysis of the deviation of the estimation on A* requests in comparison to the actual obtained number of loops.}
\end{figure}

By analyzing the Figure 5, we can see that in the first requests we have a lot of them with the 100\% of deviation, which is expected, since we haven't solved any request yet, and the load balancer will estimate by default the value 0.
But soon after, we start getting very approximate estimatives of the actual cost of the request on almost every request. This happens because we start having more and more requests solved with a lot of different areas of search, until we reach a point of having solved requests with most of the possible different areas (between 0x0 and 512x512), allowing us calculate an estimate for almost every new possible request.

In the last 100 requests, we obtained an average estimation error of ~1.12\%, which we consider a very positive result.

%------------------------------------------------------------------------- 

\subsubsection{BFS Estimate Performance}
In this test, similar to the previous one, we created a script that sent more than four hundred different requests with random (valid) values on the parameters using the BFS strategy on a map with dimensions 512x512 in order to calculate the deviation between the estimated number of loops and the actual number of loops executed on a request.

\begin{figure}
\includegraphics[width=\linewidth]{images/bfsperformance.png}
\caption{Analysis of the deviation of the estimation on BFS requests in comparison to the actual obtained number of loops.}
\end{figure}

By analyzing the Figure 6, we can see that our deviation percentage between the estimated and the actual obtained number of loops is overall higher than expected. A lot of requests have 100\% of deviation, which means that no targets were found within the search zone and therefore the load balancer doesn't calculate an estimation. But even without considering those points, from the remaining points we still have approximately 30\% of the points above 50\% of error, and most of the points below 50\% of error, are between 20~40\%. This may mean that calculating the average between all the previous seen targets is not translated into an approximate value of the distance to the real target, leading to a high deviation from the real value.

In the last 100 requests, we obtained an average estimation error of ~70\%, which we consider a bad result.

%------------------------------------------------------------------------- 

\subsubsection{DFS Estimate Performance}
In this test, similar to the previous one, we created a script that sent more than four hundred different requests with random (valid) values on the parameters using the DFS strategy on a map with dimensions 512x512 in order to calculate the deviation between the estimated number of loops and the actual number of loops executed on a request.

\begin{figure}
\includegraphics[width=\linewidth]{images/dfsperformance.png}
\caption{Analysis of the deviation of the estimation on DFS requests in comparison to the actual obtained number of loops.}
\end{figure}

By analyzing the Figure 7, we can see that our deviation percentage between the estimated and the actual obtained number of loops is overall higher than expected. We also have a lot of requests with 100\% of deviation, although they are not as many as what we had in the BFS test, which means that we had more luck on the calculated random parameters of the request. But without considering the 100\% error points, the average of the deviation between all the remaining requests is even worse when comparing to the BFS results: 60\% of those points are above 50\% of error, and from the remaining 40\% they are evenly dispersed over the Y axis.\\
This means that, most of the times, the estimated target point chosen by our algorithm (the target that appears more times on the previous requests that had target points within the search area of the actual request) is neither the real target nor a very close point to it, which makes us estimate a number of loops far from reality.

In the last 100 requests, we obtained an average estimation error of ~88\%, which we consider a bad result.

%------------------------------------------------------------------------- 

\SubSection{Future Work}
One thing that could be done to improve the estimation algorithms in general on the load balancer would be to improve the "cold start" estimation. We can see that in all the different strategies we have a very high error percentage in the first requests. If we, for instance, for BFS and DFS requests, assumed that, on average, it will take half of the zone of search to find the target point we would probably decrease that error percentage significantly.\\
The idea that we had to estimate DFS and BFS requests could probably be refined, considering the estimation errors obtained in the evaluation section.\\
The autoscaling algorithm could also be improved on a future work to instead of just considering the number of loops running at each machine at a given moment, take also into account the rate of incoming requests and/or other suitable variables.

%------------------------------------------------------------------------- 

\SubSection{Conclusions}

We consider that we have addressed all the points required by the project and that we have been able to learn and use the proposed tools during its implementation.\\
Although the results obtained in the estimation of the costs for the last two strategies were not the most favorable, we consider that it was a successful project, since that was not the main focus of the curricular unit.

%------------------------------------------------------------------------- 

\end{document}