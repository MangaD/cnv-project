#!/bin/bash

yum -y update
yum install -y java-1.7.0-openjdk-devel.x86_64

############################################################################
# Edit files ~/.bashrc and /etc/rc.local to add the following folders to   #
# Java classpath.                                                          #
############################################################################

cat classpath.txt >> /etc/rc.local
cat classpath.txt >> ~/.bashrc