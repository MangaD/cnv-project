# Setup instance

Check out [CNV AWS Guide](cnv-aws-guide.pdf) to see how to start an AWS instance and create an image of it.

## Helper scripts

1. Connect with:

```sh
connect.sh # Will ask instance IP
```

2. Install Java and set up classpath

```sh
./copy-remote # Will ask for files to copy from local PC to AWS instance. File to copy are 'setup.sh classpath.txt'
sudo ./setup.sh
# Confirm that '/etc/rc.local' and '.bashrc' contain the contents of 'classpath.txt'
```

## Setup project

1. On the AWS instance do:
```sh
git clone https://github.com/santos-samuel/cnv-project.git
wget http://sdk-for-java.amazonwebservices.com/latest/aws-java-sdk.zip
unzip aws-java-sdk.zip
```

2. Compile BIT
```sh
cd cnv-project
find . -name "*.java" | xargs javac
cd BIT/samples
```

3. Instrument solver's classes
```sh
java BranchTakenCounter ../../pt/ulisboa/tecnico/cnv/solver/. ../../pt/ulisboa/tecnico/cnv/solver/.
```

4. Create file with AWS credentials
```sh
cd
mkdir .aws
cd .aws
nano credentials # copy from site Accout Details -> AWS CLI -> Show
```

5. Run Web Server
```sh
cd ../../
java pt.ulisboa.tecnico.cnv.server.WebServer
```