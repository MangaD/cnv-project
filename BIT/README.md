## BIT - How to use

_Note: See Lab 3 for more._

1. Use java 7 version.
2. Define the classpath to use the BIT tools and options that BIT requires.

On local PC:

```sh
export CLASSPATH=$HOME/cnv-project/BIT:$HOME/cnv-project/BIT/samples:./
export _JAVA_OPTIONS="-XX:-UseSplitVerifier"
```

On remote server:

```sh
# BIT classphath
export CLASSPATH=/home/ec2-user/cnv-project/BIT:/home/ec2-user/cnv-project/BIT/samples:./
# Amazon classpath (for creating instances)
export CLASSPATH=$CLASSPATH:/home/ec2-user/aws-java-sdk-1.11.537/lib/aws-java-sdk-1.11.537.jar:/home/ec2-user/aws-java-sdk-1.11.537/third-party/lib/*:.
# Util jar to convert inputstream to bytes
export CLASSPATH=$CLASSPATH:/home/ec2-user/cnv-project/lib/commons-io-2.6/commons-io-2.6.jar
# Java option required to run the code
export _JAVA_OPTIONS="-XX:-UseSplitVerifier"
```

A BIT tool is invoked with the following syntax: 

```sh
java [Bit tool class] [class or directory to analyze or instrument] [directory to place intrumented classes] # Instrument class

java [Instrumented class] [any arguments to (instrumented) application class] # Run instrumented class

javap -c file.class # show java bytecode
```

In our case:

```sh
# Compile various solver algorithms
javac pt/ulisboa/tecnico/cnv/solver/*.java

# Instrument the classes in folder solver and put them in same folder (so override)
java BranchTakenCounter pt/ulisboa/tecnico/cnv/solver/. pt/ulisboa/tecnico/cnv/solver/.
```



Run:

```sh
java pt.ulisboa.tecnico.cnv.solver.SolverMain -d -s ASTAR -w 512 -h 512 -x0 250 -y0 250 -xS 256 -yS 256 -x1 512 -y1 512 -i $HOME/cnv-project/datasets/RANDOM_HILL_512x512_2019-03-01_10-28-39.dat -o $HOME/Desktop
```

### Access Amazon instance shell:

```sh
ssh -i CNV-lab-AWS.pem -l ec2-user instance_IP
```

### Copy our local folder to the remote server:

```sh
scp -i CNV-lab-AWS.pem -r $HOME/cnv-project/ ec2-user@<PUBLIC_DNS_IPV4>:~/
```

### Sync the remote folder with our local folder:

(Our local changes will be made in the remote server)

```sh
rsync -avL --exclude '.git' --exclude 'lib/aws-java-sdk-1.11.545/'  --progress -e "ssh -i CNV-lab-AWS.pem" -r $HOME/cnv-project/ ec2-user@<PUBLIC_DNS_IPV4>:~/cnv-project/
```

### Trigger a solver execution over an input map provided locally from the file system:

This is like simulating a request made from the website <http://groups.tecnico.ulisboa.pt/meic-cnv/cnv-project/>. 

Check `Readme.md` of project root for details.

```sh
java pt.ulisboa.tecnico.cnv.solver.SolverMain -d -s ASTAR -w 512 -h 512 -x0 250 -y0 250 -xS 256 -yS 256 -x1 300 -y1 300 -i $HOME/cnv-project/datasets/RANDOM_HILL_512x512_2019-03-01_10-28-39.dat -o $HOME/images
```

### Send request to load balancer from local bash
```sh
wget -b "http://<loadbalancerIP>:8000/climb?w=512&h=512&x0=0&x1=50&y0=0&y1=50&xS=20&yS=20&s=ASTAR&i=datasets/RANDOM_HILL_512x512_2019-02-27_09-46-42.dat" -q -O /dev/null
```