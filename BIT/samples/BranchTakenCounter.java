import BIT.highBIT.*;
import BIT.lowBIT.*;
import java.io.File;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.io.*;

public class BranchTakenCounter {
	// ConcurrentHashMap<ThreadID, LoopsMade>
	static ConcurrentHashMap<Long, Integer> chm = new ConcurrentHashMap();
	
	public static void main(String argv[]) {
		File file_in = new File(argv[0]);
        String infilenames[] = file_in.list();

        for (int i = 0; i < infilenames.length; i++) {
            String infilename = infilenames[i];
            if (infilename.endsWith(".class")) {
				// create class info object
				ClassInfo ci = new ClassInfo(argv[0] + System.getProperty("file.separator") + infilename);

				Vector routines = ci.getRoutines();

				for (Enumeration e=routines.elements();e.hasMoreElements(); ) {
						Routine routine = (Routine) e.nextElement();

						String methodName = new String(routine.getMethodName());
						if (methodName.equals("solveImage")) {
								routine.addBefore("BranchTakenCounter", "EnterMethod", methodName);
                    			routine.addAfter("BranchTakenCounter", "LeaveMethod", methodName);
                    			routine.addAfter("BranchTakenCounter", "Print", ci.getClassName());
						}
						
						Instruction[] instructions = routine.getInstructions();
						for (Enumeration b = routine.getBasicBlocks().elements(); b.hasMoreElements(); ) {
							BasicBlock bb = (BasicBlock) b.nextElement();
							Instruction instr = (Instruction)instructions[bb.getEndAddress()];
							short instr_type = InstructionTable.InstructionTypeTable[instr.getOpcode()];

							if (instr_type == InstructionTable.CONDITIONAL_INSTRUCTION)
								instr.addBefore("BranchTakenCounter", "CheckIncrement", new String("BranchOutcome"));
						}
				}
				ci.write(argv[1] + System.getProperty("file.separator") + infilename); // write instrumented class with same filename
			}
		}
	}

	public static void CheckIncrement(int brOutcome) {
		if (brOutcome == 0) {// nova iteracao
			Integer c = chm.get(Thread.currentThread().getId());
			if (c == null)
				chm.put(Thread.currentThread().getId(), 1); // initialization
			else
				chm.replace(Thread.currentThread().getId(), c + 1); // increment loops for specific thread
		}
	}

	public static void Print(String foo) {
		try {
            File log = new File("logs/log" + Thread.currentThread().getId() + ".txt");

            //Delete previous thread file (if exists)
            log.delete();

            log.createNewFile();

            PrintWriter pw = new PrintWriter(new FileWriter(log));
            pw.println(chm.get(Thread.currentThread().getId())); // write number of loops made during the request
            pw.close();

        } catch (IOException e) {
            System.out.println("Something went wrong. The app will now exit.");
            System.exit(1);
        }

		chm.replace(Thread.currentThread().getId(), 0); // reset counter for this thread
	}

	public static void EnterMethod(String s) {
        System.out.println("method: " + s);
    }
    
    public static void LeaveMethod(String s) {
        System.out.println("stat for method: " + s);
    }
}